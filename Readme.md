# Alice diary

## текст

Алиса учится в 9 классе и любит читать книжки по криптографии. Недавно она придумала собственную криптосистему, которую, по ее мнению, невозможно взломать. И правда, Ева, которая решила прочитать личный дневник Алисы ничего не смогла в нем разобрать, ведь теперь Алиса шифрует все записи в своем дневнике с помощью программы, написанной ею, которая шифрует текст по придуманному ею алгоритму.

***

Alice studies in 9th grade and likes to read books on cryptography. Recently she invented her own cryptosystem, which in her opinion is impossible to crack. It is true that Eva, who decided to read Alice’s personal diary, couldn’t make anything out of it, because now Alice encrypts all the entries in her diary using a program written by her, which encrypts the text according to the algorithm she invented.


## Подсказка

флаг в формате mctf{}

## Решение

В целом криптосистема выглядит следующе

У нас есть ключ из 6 значений ABCDEF, каждое значение от 33 до 127

входящее число шифруется по правилу

``` math
A + Bx + Cx^2 + Dx^3 + Ex^4 + Fx^5
```

Решением является решение системы полиномов с нахождением коэфицентов
для решения необхожимо знать 6 различных символов `mctf{}` отлично для этого

составим систему полиномов
    
``` math
\begin{cases}
A + Bord(m) + Cord(m)^2 + Dord(m)^3 + Eord(m)^4 + Ford(m)^5 = шифротекст[0]
\\ A + Bord(с) + Cord(c)^2 + Dord(c)^3 + Eord(c)^4 + Ford(c)^5 = шифротекст[1]
\\ A + Bord(t) + Cord(t)^2 + Dord(t)^3 + Eord(t)^4 + Ford(t)^5 = шифротекст[2]
\\ A + Bord(f) + Cord(f)^2 + Dord(f)^3 + Eord(f)^4 + Ford(f)^5 = шифротекст[3]
\\ A + Bord(\{) + Cord(\{)^2 + Dord(\{)^3 + Eord(\{)^4 + Ford(\{)^5 = шифротекст[4]
\\ A + Bord(\}) + Cord(\})^2 + Dord(\})^3 + Eord(\})^4 + Ford(\})^5 = шифротекст[-1]
\end{cases}
```

решаем СЛАУ и находим неизвестные коэфиценты - ключ

``` python
import numpy as np

C = []
with open('diary/flag', 'rb') as f:
    while data := f.read(6):
        C.append(int.from_bytes(data, 'big'))

P = [ord(i) for i in 'mctf{}']
M = np.array([[P[i]**j for j in range(6)] for i in range(6)])
v = np.array(C[:5] + C[-1:])
X = np.round(np.linalg.solve(M, v)).astype(int)

print(''.join(map(chr, X)))
```
